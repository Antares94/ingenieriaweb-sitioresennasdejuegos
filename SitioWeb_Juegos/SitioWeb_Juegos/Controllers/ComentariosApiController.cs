﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace SitioWeb_Juegos.Controllers
{
    public class ComentariosApiController : ApiController
    {
        private BDResennasJuegosEntities db = new BDResennasJuegosEntities();

        // GET: api/ComentariosApi
        public IQueryable<Comentarios> GetComentarios()
        {
            db.Configuration.ProxyCreationEnabled = false; //Para que no de error de proxy
            return db.Comentarios;
        }

        // GET: api/ComentariosApi/5
        [ResponseType(typeof(Comentarios))]
        public async Task<IHttpActionResult> GetComentarios(int id)
        {
            Comentarios comentarios = await db.Comentarios.FindAsync(id);
            if (comentarios == null)
            {
                return NotFound();
            }

            return Ok(comentarios);
        }

        // PUT: api/ComentariosApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutComentarios(int id, Comentarios comentarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != comentarios.Id)
            {
                return BadRequest();
            }

            db.Entry(comentarios).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComentariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ComentariosApi
        [ResponseType(typeof(Comentarios))]
        public async Task<IHttpActionResult> PostComentarios(Comentarios comentarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Comentarios.Add(comentarios);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = comentarios.Id }, comentarios);
        }

        // DELETE: api/ComentariosApi/5
        [ResponseType(typeof(Comentarios))]
        public async Task<IHttpActionResult> DeleteComentarios(int id)
        {
            Comentarios comentarios = await db.Comentarios.FindAsync(id);
            if (comentarios == null)
            {
                return NotFound();
            }

            db.Comentarios.Remove(comentarios);
            await db.SaveChangesAsync();

            return Ok(comentarios);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ComentariosExists(int id)
        {
            return db.Comentarios.Count(e => e.Id == id) > 0;
        }
    }
}