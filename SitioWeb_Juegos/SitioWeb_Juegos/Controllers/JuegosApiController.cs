﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Datos;

namespace SitioWeb_Juegos.Controllers
{
    public class JuegosApiController : ApiController
    {
        private BDResennasJuegosEntities db = new BDResennasJuegosEntities();

        // GET: api/JuegosApi
        public IQueryable<Juegos> GetJuegos()
        {
            db.Configuration.ProxyCreationEnabled = false; //Para que no de error de proxy
            return db.Juegos;
        }

        // GET: api/JuegosApi/5
        [ResponseType(typeof(Juegos))]
        public async Task<IHttpActionResult> GetJuegos(int id)
        {
            Juegos juegos = await db.Juegos.FindAsync(id);
            if (juegos == null)
            {
                return NotFound();
            }

            return Ok(juegos);
        }

        // PUT: api/JuegosApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutJuegos(int id, Juegos juegos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != juegos.Id)
            {
                return BadRequest();
            }

            db.Entry(juegos).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JuegosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/JuegosApi
        [ResponseType(typeof(Juegos))]
        public async Task<IHttpActionResult> PostJuegos(Juegos juegos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Juegos.Add(juegos);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = juegos.Id }, juegos);
        }

        // DELETE: api/JuegosApi/5
        [ResponseType(typeof(Juegos))]
        public async Task<IHttpActionResult> DeleteJuegos(int id)
        {
            Juegos juegos = await db.Juegos.FindAsync(id);
            if (juegos == null)
            {
                return NotFound();
            }

            db.Juegos.Remove(juegos);
            await db.SaveChangesAsync();

            return Ok(juegos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JuegosExists(int id)
        {
            return db.Juegos.Count(e => e.Id == id) > 0;
        }
    }
}