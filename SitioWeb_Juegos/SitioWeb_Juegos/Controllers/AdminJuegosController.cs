﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Datos;
using Servicios;

namespace SitioWeb_Juegos.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class AdminJuegosController : Controller
    {
        private ConsultasJuegos consultasJuegos = new ConsultasJuegos();
        private ConsultasCategorias consultasCategorias = new ConsultasCategorias();
        // GET: AdminJuegos
        public ActionResult Index()
        {
            var model = consultasJuegos.ObtenerTodosLosJuegos();
            return View(model);
        }


        // GET: AdminJuegos/Details/5
        public ActionResult Details(int id)
        {
            var model = consultasJuegos.ObtenerJuegoPorId(id);

            if (model == null)
                return RedirectToAction("Index");

            return View(model);
        }

        // GET: AdminJuegos/Create
        public ActionResult Create()
        {
            ViewBag.Categorias = new SelectList(consultasCategorias.ObtenerCategorias(), "Id", "Descripcion");
            return View();
        }

        // POST: AdminJuegos/Create
        [HttpPost]
        public ActionResult Create(Juego model, HttpPostedFileBase Imagen)
        {
            ViewBag.Categorias = new SelectList(consultasCategorias.ObtenerCategorias(), "Id", "Descripcion");
            try
            {
                // TODO: Add insert logic here
                try
                {
                    if (Imagen != null)
                    {
                        WebImage img = new WebImage(Imagen.InputStream);
                        FileInfo imageninfo = new FileInfo(Imagen.FileName);

                        string nuevaimagen = Guid.NewGuid().ToString() + imageninfo.Extension;
                        img.Resize(900, 300, false);
                        img.Save("~/Imagenes/ImagenJuegos/" + nuevaimagen);
                        model.Imagen = "/Imagenes/ImagenJuegos/" + nuevaimagen;
                    }
                 
                    var idGenerado = consultasJuegos.CrearJuego(model);

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminJuegos/Edit/5
        public ActionResult Edit(int id)
        {
            var model = consultasJuegos.ObtenerJuegoPorId(id);
            ViewBag.Categorias = new SelectList(consultasCategorias.ObtenerCategorias(), "Id", "Descripcion",model.IdCategoria);
            if (model == null)
                return RedirectToAction("Index");

            return View(model);
        }

        // POST: AdminJuegos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Juego model, HttpPostedFileBase Imagen)
        {
            ViewBag.Categorias = new SelectList(consultasCategorias.ObtenerCategorias(), "Id", "Descripcion", model.IdCategoria);
            try
            {
                if (Imagen != null)
                {
                    if (System.IO.File.Exists(Server.MapPath(model.Imagen)))
                    {
                        System.IO.File.Delete(Server.MapPath(model.Imagen));
                    }
                    WebImage img = new WebImage(Imagen.InputStream);
                    FileInfo fotoinfo = new FileInfo(Imagen.FileName);

                    string nuevafoto = Guid.NewGuid().ToString() + fotoinfo.Extension;
                    img.Resize(900, 300, false);
                    img.Save("~/Imagenes/ImagenJuegos/" + nuevafoto);
                    model.Imagen = "/Imagenes/ImagenJuegos/" + nuevafoto;
              
                }
                    model.Id = id;
                var idGenerado = consultasJuegos.ModificarJuego(model, false);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminJuegos/Delete/5
        public ActionResult Delete(int id)
        {
            var model = consultasJuegos.ObtenerJuegoPorId(id);

            if (model == null)
                return RedirectToAction("Index");

            return View(model);
        }

        // POST: AdminJuegos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Juego model)
        {

            try
            {
                model.Id = id;
                var idGenerado = consultasJuegos.ModificarJuego(model, true);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }





        //        // GET: AdminJuegos
        //        public ActionResult Index()
        //        {
        //            var juegos = db.Juegos.Include(j => j.Categorias);
        //            return View(juegos.ToList());
        //        }

        //        // GET: AdminJuegos/Details/5
        //        public ActionResult Details(int? id)
        //        {
        //            if (id == null)
        //            {
        //                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            }
        //            Juegos juegos = db.Juegos.Find(id);
        //            if (juegos == null)
        //            {
        //                return HttpNotFound();
        //            }
        //            return View(juegos);
        //        }

        //        // GET: AdminJuegos/Create
        //        public ActionResult Create()
        //        {
        //            ViewBag.IdCategoria = new SelectList(db.Categorias, "Id", "Descripcion");
        //            return View();
        //        }

        //        // POST: AdminJuegos/Create
        //        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //        [HttpPost]
        //        [ValidateAntiForgeryToken]
        //        public ActionResult Create([Bind(Include = "Id,Descripcion,IdCategoria,Imagen")]Juegos juegos, HttpPostedFileBase Imagen)
        //        {
        //            if (ModelState.IsValid)
        //            {

        //                if (Imagen != null)
        //                {
        //                    WebImage img = new WebImage(Imagen.InputStream);
        //                    FileInfo fotoinfo = new FileInfo(Imagen.FileName);

        //                    string nuevafoto = Guid.NewGuid().ToString() + fotoinfo.Extension;
        //                    img.Resize(900, 300, false);
        //                    img.Save("~/Imagenes/ImagenJuegos/" + nuevafoto);
        //                    juegos.Imagen = "/Imagenes/ImagenJuegos/" + nuevafoto;

        //                }
        //                db.Juegos.Add(juegos);
        //                db.SaveChanges();
        //                return RedirectToAction("Index");
        //            }

        //            ViewBag.IdCategoria = new SelectList(db.Categorias, "Id", "Descripcion", juegos.IdCategoria);
        //            return View(juegos);
        //        }

        //        // GET: AdminJuegos/Edit/5
        //        public ActionResult Edit(int? id)
        //        {
        //            if (id == null)
        //            {
        //                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            }
        //            Juegos juegos = db.Juegos.Find(id);
        //            if (juegos == null)
        //            {
        //                return HttpNotFound();
        //            }
        //            ViewBag.IdCategoria = new SelectList(db.Categorias, "Id", "Descripcion", juegos.IdCategoria);
        //            return View(juegos);
        //        }

        //        // POST: AdminJuegos/Edit/5
        //        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //        [HttpPost]
        //        [ValidateAntiForgeryToken]
        //        public ActionResult Edit([Bind(Include = "Id,Descripcion,IdCategoria,Imagen")]Juegos juegos,int id, HttpPostedFileBase Imagen)
        //        {

        //            try
        //            {
        //                Juegos juego = db.Juegos.Find(id);
        //                if (Imagen != null)
        //                {
        //                    if (System.IO.File.Exists(Server.MapPath(juego.Imagen)))
        //                    {
        //                        System.IO.File.Delete(Server.MapPath(juego.Imagen));
        //                    }
        //                    WebImage img = new WebImage(Imagen.InputStream);
        //                    FileInfo fotoinfo = new FileInfo(Imagen.FileName);

        //                    string nuevafoto = Guid.NewGuid().ToString() + fotoinfo.Extension;
        //                    img.Resize(900, 300, false);
        //                    img.Save("~/Imagenes/ImagenJuegos/" + nuevafoto);
        //                    juego.Imagen = "/Imagenes/ImagenJuegos/" + nuevafoto;
        //                    juego.IdCategoria = juegos.IdCategoria;
        //                    juego.Descripcion = juegos.Descripcion;
        //                    db.Entry(juego).State = EntityState.Modified;
        //                    db.SaveChanges();
        //                }

        //            }
        //            catch 
        //            {
        //                return View();              
        //            }
        //            //if (ModelState.IsValid)
        //            //{
        //            //    db.Entry(juegos).State = EntityState.Modified;
        //            //    db.SaveChanges();
        //            //    return RedirectToAction("Index");
        //            //}
        //            ViewBag.IdCategoria = new SelectList(db.Categorias, "Id", "Descripcion", juegos.IdCategoria);
        //            return View(juegos);
        //        }

        //        // GET: AdminJuegos/Delete/5
        //        public ActionResult Delete(int? id)
        //        {
        //            if (id == null)
        //            {
        //                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            }
        //            Juegos juegos = db.Juegos.Find(id);
        //            if (juegos == null)
        //            {
        //                return HttpNotFound();
        //            }
        //            return View(juegos);
        //        }

        //        // POST: AdminJuegos/Delete/5
        //        [HttpPost, ActionName("Delete")]
        //        [ValidateAntiForgeryToken]
        //        public ActionResult DeleteConfirmed(int id)
        //        {
        //            Juegos juegos = db.Juegos.Find(id);
        //            db.Juegos.Remove(juegos);
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }

        //        public ActionResult _ListaJuegosBuscador()
        //        {

        //            ViewBag.Juegos = new SelectList(consultasJuegos.ObtenerJuegos(), "Id", "Descripcion");
        //            return PartialView("_ListaJuegosBuscador");
        //        }

        //        protected override void Dispose(bool disposing)
        //        {
        //            if (disposing)
        //            {
        //                db.Dispose();
        //            }
        //            base.Dispose(disposing);
        //        }
    }
}
