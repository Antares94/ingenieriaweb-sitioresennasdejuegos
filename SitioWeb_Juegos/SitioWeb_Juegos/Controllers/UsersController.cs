﻿using Microsoft.AspNet.Identity;
using Servicios;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace SitioWeb_Juegos.Controllers
{
    public class UsersController : Controller
    {
        public ConsultasUsuarios ConsultasUsuarios = new ConsultasUsuarios();

        // GET: Users
        [Authorize]
        public ActionResult Index(string username)
        {
            var model = ConsultasUsuarios.ObtenerUsuarioPorUsername(username);
            return View(model);
        }

        // GET: Users/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Edit/5
        [Authorize]
        public ActionResult Edit()
        {
            var id = User.Identity.GetUserId();
            var model = ConsultasUsuarios.ObtenerUsuarioPorId(id);         
            if (model == null)
                return RedirectToAction("Index");

            return View(model);        
        }

        // POST: Users/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult Edit(Usuario model, HttpPostedFileBase Avatar)
        {
            
            try
            {
                if (Avatar != null)
                {
                    if (System.IO.File.Exists(Server.MapPath(model.Avatar)))
                    {
                        System.IO.File.Delete(Server.MapPath(model.Avatar));
                    }
                    WebImage img = new WebImage(Avatar.InputStream);
                    FileInfo fotoinfo = new FileInfo(Avatar.FileName);

                    string nuevafoto = Guid.NewGuid().ToString() + fotoinfo.Extension;
                    img.Resize(500, 500, false);
                    img.Save("~/Imagenes/ImagenUsuario/" + nuevafoto);
                    model.Avatar = "/Imagenes/ImagenUsuario/" + nuevafoto;

                }
                var id = User.Identity.GetUserId();
                model.Id = id;
                var idGenerado = ConsultasUsuarios.ModificarPerfilUsuario(model);
                return View(model);
            }
            catch
            {
                return View();
            }

        }

        // GET: Users/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
