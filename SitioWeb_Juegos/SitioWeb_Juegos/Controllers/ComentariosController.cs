﻿using Microsoft.AspNet.Identity;
using Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SitioWeb_Juegos.Controllers
{
    public class ComentariosController : Controller
    {
        private ConsultasComentarios consultasComentarios = new ConsultasComentarios();
        private ConsultasEstados consultasEstados = new ConsultasEstados();
        // GET: Comentarios
        public ActionResult Index()
        {
            var model = consultasComentarios.ObtenerComentarios();
            return View(model);         
        }

        public ActionResult Edit(int id)
        {          
            var model = consultasComentarios.ObtenerComentarioPorId(id);
            ViewBag.Estados = new SelectList(consultasEstados.ObtenerEstados(), "Id", "Descripcion", model.IdEstado);
            if (model == null)
                return RedirectToAction("Index");

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, Comentario model)
        {
            ViewBag.Estados = new SelectList(consultasEstados.ObtenerEstados(), "Id", "Descripcion", model.IdEstado);
            try
            {
                model.Id = id;
                var idGenerado = consultasComentarios.ModificarComentario(model, false);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult CrearComentario(string comentario, int IdPost)
        {
            ConsultasComentarios consultascomentarios = new ConsultasComentarios();
            var IdAutor = User.Identity.GetUserId();

            Comentario model = new Comentario();
            model.Autor = IdAutor;
            model.Contenido = comentario;
            model.Post = IdPost;
            var idgenerado = consultascomentarios.CrearComentario(model);
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EliminarComentario(int IdComentario, int IdPost)
        {
            ConsultasComentarios consultascomentarios = new ConsultasComentarios();

            Comentario model = new Comentario();
            model.Id = IdComentario;
            model.Post = IdPost;
            var idgenerado = consultascomentarios.ModificarComentario(model,true);
            return Json(false, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult EliminarComentario(int Id)
        //{
        //    ConsultasComentarios consultascomentarios = new ConsultasComentarios();
        //    Comentario model = new Comentario();
        //    model.Id = Id;
        //    var idpost = consultascomentarios.ModificarComentario(model, true);

        //    return RedirectToAction("Details", "Posts", new { id = idpost });
        //}
    }
}