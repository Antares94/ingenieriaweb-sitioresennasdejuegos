﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using System.Data.Entity;


namespace SitioWeb_Juegos.Controllers
{
    public class HomeController : Controller
    {
        private BDResennasJuegosEntities db = new BDResennasJuegosEntities();
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.AspNetUsers).Include(p => p.Estados).Include(p => p.Juegos).Where(x => x.Estados.Descripcion == "Activo");                 
            return View(posts.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}