﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Datos;
using Microsoft.AspNet.Identity;
using Servicios;

namespace SitioWeb_Juegos.Controllers
{
    public class DenunciasPostsController : Controller
    {
        private BDResennasJuegosEntities db = new BDResennasJuegosEntities();
        private ConsultasDenuncias consultasDenuncias = new ConsultasDenuncias();
        private ConsultasMotivos consultasMotivos = new ConsultasMotivos();
        public ActionResult Index()
        {
            var model = consultasDenuncias.ObtenerPostsDenunciados();
            return View(model);
        }
        // GET: Denuncias/Create
        public ActionResult Create()
        {
            ViewBag.Motivos = new SelectList(consultasMotivos.ObtenerMotivosDenuncias(), "Id", "Descripcion");
            return View();
        }

        // POST: Denuncias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PostDenunciado model,int IdPost)
        {
            ConsultasDenuncias consultasdenuncias = new ConsultasDenuncias();
            var IdDenunciante = User.Identity.GetUserId();       
            model.IdUsuario = IdDenunciante;
            model.IdPost = IdPost;
            var idgenerado = consultasdenuncias.CrearDenunciaPost(model);
            //return View(model);

            return RedirectToAction("Index", "Posts");

        }

        public ActionResult CrearDenunciaComentario(string descripcion, int id, int motivo)
        {

            ConsultasDenuncias consultasdenuncias = new ConsultasDenuncias();
            var IdDenunciante = User.Identity.GetUserId();
            ComentarioDenunciado model = new ComentarioDenunciado();
            model.IdUsuario = IdDenunciante;
            model.Descripcion = descripcion;
            model.IdComentario = id;
            model.IdMotivo = motivo;
            var idgenerado = consultasdenuncias.CrearDenunciaComentario(model);

            return RedirectToAction("Details", "Posts" /*new { id = idpost }*/);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
