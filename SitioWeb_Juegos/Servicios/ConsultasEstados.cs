﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class ConsultasEstados
    {
        BDResennasJuegosEntities db = new BDResennasJuegosEntities();

        public List<Estado> ObtenerEstados()
        {
            var estados = db.Estados;
            List<Estado> resultado = new List<Estado>();
            foreach (var item in estados.ToList())
            {
                resultado.Add(new Estado()
                {
                    Id = item.Id,
                    Descripcion = item.Descripcion
                });
            }
            return resultado;
        }

        public Estado ObtenerEstadoPorId(int id)
        {
            var estado = db.Estados.Where(x => x.Id == id).FirstOrDefault();
            Estado resultado = (estado == null) ? null : new Estado()
            {
                Id = estado.Id,
                Descripcion = estado.Descripcion,
            };

            return resultado;
        }

        public int CrearEstado(Estado model)
        {
            Estados nuevoestado = new Estados();
            nuevoestado.Descripcion = model.Descripcion;
            db.Estados.Add(nuevoestado);
            db.SaveChanges();
            int idgenerado = nuevoestado.Id;
            return (idgenerado);
        }

        public int ModificarEstado(Estado model, bool eliminar)
        {
            Estados estadoActual = db.Estados.Where(x => x.Id == model.Id).SingleOrDefault();

            if (estadoActual != null)
            {
                if (eliminar)
                {
                }
                else
                {
                    estadoActual.Descripcion = model.Descripcion;
                }
                db.SaveChanges();
            }
            return estadoActual.Id;
        }
    }
}
