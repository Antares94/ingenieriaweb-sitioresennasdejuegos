﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class ConsultasRolesUsuario
    {
        BDResennasJuegosEntities db = new BDResennasJuegosEntities();

        public List<RolUsuario> ObtenerRoles()
        {
            var roles = db.AspNetRoles;
            List<RolUsuario> resultado = new List<RolUsuario>();
            foreach (var item in roles.ToList())
            {
                resultado.Add(new RolUsuario()
                {
                    Id = item.Id,
                    Descripcion = item.Name
                });
            }
            return resultado;
        }
    }
}
