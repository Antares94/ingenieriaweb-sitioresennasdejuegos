﻿using Datos;
using System;

namespace Servicios
{
    public class PostDenunciado
    {
        public int Id { get; set; }
        public Nullable<int> IdPost { get; set; }
        public string IdUsuario { get; set; }
        public Nullable<int> IdMotivo { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Descripcion { get; set; }

        public virtual MotivoDenuncia Motivo { get; set; }
        public virtual Usuario Usuario { get; set; }

    }
}