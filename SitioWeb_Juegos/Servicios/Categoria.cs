﻿using System;

namespace Servicios
{
    public class Categoria
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public Nullable<bool> Eliminado { get; set; }
    }
}