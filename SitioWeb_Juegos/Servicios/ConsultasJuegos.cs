﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class ConsultasJuegos
    {
        BDResennasJuegosEntities db = new BDResennasJuegosEntities();

        public List<Juego> ObtenerTodosLosJuegos()
        {
            var juegos = db.Juegos;
            List<Juego> resultado = new List<Juego>();
            foreach (var item in juegos.ToList())
            {
                resultado.Add(new Juego()
                {
                    Id = item.Id,
                    Descripcion = item.Descripcion,
                    Eliminado = item.Eliminado,
                    Categorias = new Categoria { Id= item.Categorias.Id, Descripcion=item.Categorias.Descripcion},
                    Imagen = item.Imagen


                });
            }
            return resultado;
        }

        public List<Juego> ObtenerJuegos()
        {
            var juegos = db.Juegos.Where(x => x.Eliminado == false);
            List<Juego> resultado = new List<Juego>();
            foreach (var item in juegos.ToList())
            {
                resultado.Add(new Juego()
                {
                    Id = item.Id,
                    Descripcion = item.Descripcion,
                    Eliminado = item.Eliminado,
                    Categorias = new Categoria { Id = item.Categorias.Id, Descripcion = item.Categorias.Descripcion },
                    Imagen = item.Imagen

                });
            }
            return resultado;
        }

        public Juego ObtenerJuegoPorId(int id)
        {
            var juego = db.Juegos.Where(x => x.Id == id).FirstOrDefault();
            Juego resultado = (juego == null) ? null : new Juego()
            {
                Id = juego.Id,
                Descripcion = juego.Descripcion,
                Imagen = juego.Imagen,
                Eliminado = juego.Eliminado,
                IdCategoria = juego.IdCategoria
                
            };

            return resultado;
        }

        public int CrearJuego(Juego model)
        {
            Juegos nuevojuego = new Juegos();
            nuevojuego.Descripcion = model.Descripcion;
            nuevojuego.Eliminado = false;
            nuevojuego.Imagen = model.Imagen;
            nuevojuego.IdCategoria = model.IdCategoria;
            db.Juegos.Add(nuevojuego);
            db.SaveChanges();
            int idgenerado = nuevojuego.Id;
            return (idgenerado);
        }

        public int ModificarJuego(Juego model, bool eliminar)
        {
            Juegos juegoActual = db.Juegos.Where(x => x.Id == model.Id).SingleOrDefault();

            if (juegoActual != null)
            {
                if (eliminar)
                {
                    juegoActual.Eliminado = true;
                }
                else
                {
                    if (model.Imagen != null)
                    {
                        juegoActual.Imagen = model.Imagen;
                    }
                    juegoActual.Descripcion = model.Descripcion;
                    juegoActual.IdCategoria = model.IdCategoria;
                    juegoActual.Eliminado = model.Eliminado;
                    
                }
                db.SaveChanges();
            }
            return juegoActual.Id;
        }
    }
}
