﻿using System;
using System.Collections.Generic;

namespace Servicios
{
    public class Juego
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int IdCategoria { get; set; }
        public string Imagen { get; set; }
        public Nullable<bool> Eliminado { get; set; }

        public virtual Categoria Categorias { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}