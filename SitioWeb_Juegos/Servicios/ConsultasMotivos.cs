﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class ConsultasMotivos
    {
        BDResennasJuegosEntities db = new BDResennasJuegosEntities();

        public List<MotivoDenuncia> ObtenerMotivosDenuncias()
        {
            var motivos = db.MotivosDenuncia;
            List<MotivoDenuncia> resultado = new List<MotivoDenuncia>();
            foreach (var item in motivos.ToList())
            {
                resultado.Add(new MotivoDenuncia()
                {
                    Id = item.Id,
                    Descripcion = item.Descripcion
                });
            }
            return resultado;
        }

        public MotivoDenuncia ObtenerMotivoDenunciaPorId(int id)
        {
            var motivo = db.MotivosDenuncia.Where(x => x.Id == id).FirstOrDefault();
            MotivoDenuncia resultado = (motivo == null) ? null : new MotivoDenuncia()
            {
                Id = motivo.Id,
                Descripcion = motivo.Descripcion,
            };

            return resultado;
        }

        public int CrearMotivoDenuncia(MotivoDenuncia model)
        {
            MotivosDenuncia nuevomotivo = new MotivosDenuncia();
            nuevomotivo.Descripcion = model.Descripcion;
            db.MotivosDenuncia.Add(nuevomotivo);
            db.SaveChanges();
            int idgenerado = nuevomotivo.Id;
            return (idgenerado);
        }

        public int ModificarMotivoDenuncia(MotivoDenuncia model, bool eliminar)
        {
            MotivosDenuncia motivoActual = db.MotivosDenuncia.Where(x => x.Id == model.Id).SingleOrDefault();

            if (motivoActual != null)
            {
                if (eliminar)
                {
                }
                else
                {
                    motivoActual.Descripcion = model.Descripcion;
                }
                db.SaveChanges();
            }
            return motivoActual.Id;
        }
    }
}
